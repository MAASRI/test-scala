import React, { useState } from 'react';
import Button from 'ui/build/components/Button/Button';
import Para from 'ui/build/components/Para/Para';
import { useHistory } from 'react-router-dom';
import Popover from 'react-bootstrap/Popover';
import OverlayTrigger from 'react-bootstrap/OverlayTrigger';
import RenderIcon from '../RheaSideNav/Icons/RenderIcon';
import './PopOver.scss';

const PopOver = props => {
  const { openEditProfile, userData } = props;
  const history = useHistory();
  const [userFect, setUserFect] = useState(false);
  const [showPopOver, setshowPopOver] = useState(false);

  const triggerPopOver = () => {
    if (showPopOver === true) {
      setshowPopOver(false);
      return;
    }
    setshowPopOver(true);
  }

  return (
    <React.Fragment>
      {['bottom'].map(placement => (
        <OverlayTrigger
          trigger="click"
          onToggle={triggerPopOver}
          show={showPopOver}
          key={placement}
          rootClose
          placement={placement}
          overlay={
            <Popover id={`popover-positioned-${placement}`}>
              <Popover.Content>
                <div className="d-flex justify-content-between">
                  <div className="image-container">
                    <div className="image-centerer">
                      {
                          
                            userData.userPicURL &&
                            (userData.userPicURL).trim() !== ""
                          ?
                          <img src={`/assets/${userData.userPicURL}.png`} alt="image" />
                          :
                          <img src="/assets/Default.png" alt="image" />
                      }
                    </div>
                  </div>
                  <div className="profile-details">
                    <p className="name">{userData.name}</p>
                    <p className="mail"> {userData.emailId}</p>
                    {!userFect ? (
                      <div className="edit-profile">
                        <Button
                          onClick={() => {
                            setshowPopOver(false);
                            openEditProfile()
                          }}
                          className="btn edit-btn" varient="primary">
                          Edit Profile
                        </Button>
                      </div>
                    ) : null}
                  </div>
                </div>
                <div className="line"></div>
                <div style={{ paddingLeft: '11px', paddingTop: '5px' }}>
                  <Para
                    onClick={() => {
                      localStorage.clear();
                      history.push('/');
                      window.location.reload(false);
                    }}
                  >
                    <RenderIcon icon="RiLogoutBoxRLine" /> &nbsp; Logout
                  </Para>
                </div>
              </Popover.Content>
            </Popover>
          }
        >
          <div className="logout-img">
            {
              userData.userPicURL &&
              (userData.userPicURL).trim() !== ""
              ?
                <img src={`/assets/${userData.userPicURL}.png`} alt="image" />
                :
                <img src="/assets/Default.png" alt="image" />
            }
          </div>
        </OverlayTrigger>
      ))}
    </React.Fragment>
  );
};
export default PopOver;