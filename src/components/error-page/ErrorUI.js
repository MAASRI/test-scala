import React, { useEffect, useState } from 'react'
import { MdError } from 'react-icons/md'
import Para from 'ui/build/components/Para/Para';
import Heading from 'ui/build/components/Heading/Heading';
import Button from 'ui/build/components/Button/Button';
import { useHistory } from 'react-router-dom';
import './ErrorUI.scss'


// This is fallback UI when something goes wrong
const ErrorUI = ({ errorMessage, errorStack }) => {
    const [showStackInfo, setShowStackInfo] = useState(false)
    const history = useHistory()
    const goBack = () => {
        history.goBack()
    }
    useEffect(() => {
        console.log('error', errorMessage, errorStack);
    }, [errorMessage, errorStack])
    return (
        <>

            <div className="error-fallback-page-container">
                <div className="error-inner-section">
                    <MdError className="error-icon" color="#16617D" size='120px'></MdError>
                    <Heading className="error-heading" variant="h5">{errorMessage}</Heading>
                    <div className="btn-section">
                        <Button onClick={goBack} variant="primary">Go Back</Button>
                        <Button variant="outline" onClick={() => setShowStackInfo(!showStackInfo)}>Check Error Stack</Button>
                    </div>
                    {
                        showStackInfo &&
                        <Para className="error-stack">{errorStack}</Para>
                    }
                </div>
            </div>
        </>
    )
}

export default ErrorUI
