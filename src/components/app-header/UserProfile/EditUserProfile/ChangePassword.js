import React, { useState } from 'react';
import Input from "ui/build/components/Inputs/Input";
import Button from 'ui/build/components/Button/Button';
import Alert from "ui/build/components/Alert/Alert";
import Accrodion from "ui/build/components/Accordion/Accordion";
import { Formik, Form } from 'formik';
import * as Yup from 'yup';
import { changePasswordMethod } from './EditUserProfile.service';
import { useHistory } from 'react-router-dom';

import browserBunyan from "rr-js/lib/browserBunyan.js";
import { v4 as uuid } from "uuid";
import { SERVICE_NAME } from "../../../../utils/StaticData";

const passowrdInitialState = {
    currentPassword: "",
    newPassword: "",
    confirmNewPassword: "",
};

function ChangePassword() {
    const logger = browserBunyan({ name: SERVICE_NAME });
    const history = useHistory();
    const [passowrdInit, setPassowrdInit] = useState(passowrdInitialState);
    const [showAlert, setShowAlert] = useState(false);
    const [toast, setToast] = useState({
        alertType: "",
        message: "",
    });

    const formValodations = Yup.object().shape({
        currentPassword: Yup.string()
            .required('Current Password is required!'),
        newPassword: Yup.string()
            .matches(/^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,}$/, 'New Password must contain 8 characters, one alphabet, one number and one special case character')
            .notOneOf([Yup.ref('currentPassword')], 'Current Password and New Password should not be same!')
            .required('New Password is required!'),
        confirmNewPassword: Yup.string()
            .oneOf([Yup.ref('newPassword')], 'New Password and Confirm New Password must be same!')
            .required('Confirm New Password is required!'),
    })

    const customHandleChange = (e, setFieldValue) => {
        setFieldValue(e.target.name, e.target.value)
        setPassowrdInit({
            ...passowrdInit,
            [e.target.name]: e.target.value
        })
    }

    const resetFun = () => {
        setPassowrdInit(passowrdInitialState);
    }

    return (
        <div>
            <Accrodion
                variant="monad-plus"
                label="Change Password"
                icon=""
                id="changePassword"
            >
                <Formik
                    initialValues={passowrdInit}
                    validationSchema={formValodations}
                    enableReinitialize={true}
                    onSubmit={(data, { setSubmitting }) => {
                        const updateBody = {};
                        updateBody['userId'] = JSON.parse(localStorage.getItem('loginResponse')).responseData.userId;
                        updateBody['currentPassword'] = data.currentPassword;
                        updateBody['newPassword'] = data.newPassword;
                        updateBody['confirmNewPassword'] = data.confirmNewPassword;
                        changePasswordMethod(updateBody)
                            .then(resp => {
                                if (resp.data.statusCode === 200) {
                                    const rrid = uuid();
                                    logger.info(rrid, SERVICE_NAME, "Change password API success", {
                                        userId: updateBody['userId']
                                    });

                                    resetFun();
                                    setShowAlert(true)
                                    setToast({
                                        alertType: 'success',
                                        message: resp.data.message
                                    })
                                    setTimeout(() => {
                                        history.push('/');
                                        localStorage.clear();
                                        window.location.reload(false);
                                    }, 2000);
                                }
                            })
                            .catch(err => {
                                const rrid = uuid();
                                logger.error(rrid, SERVICE_NAME, "Change password API error", {
                                    userId: updateBody['userId'],
                                    error: err
                                });
                                setShowAlert(true)
                                setToast({
                                    alertType: 'danger',
                                    message: err.response.data.message
                                })
                            })
                        setSubmitting(false);

                    }}
                >
                    {({
                        values,
                        touched,
                        errors,
                        setFieldValue,
                        handleBlur,
                        isSubmitting,
                        handleSubmit
                    }) => (
                        <Form onSubmit={handleSubmit}>
                            <div className="row">
                                <div className="col-6">
                                    <Input type="password"
                                        placeholder="Current Password"
                                        name="currentPassword"
                                        label="Current Password"
                                        isMandatory
                                        autoComplete="off"
                                        value={values.currentPassword}
                                        onChange={e => customHandleChange(e, setFieldValue)}
                                        onBlur={handleBlur}
                                    />
                                    {touched.currentPassword && errors.currentPassword && (
                                        <div className="error">
                                            {errors.currentPassword}
                                        </div>
                                    )}
                                </div>
                                <div className="col-6">
                                    <Input type="password"
                                        placeholder="New Password"
                                        name="newPassword"
                                        label="New Password"
                                        isMandatory
                                        autoComplete="off"
                                        value={values.newPassword}
                                        onChange={e => customHandleChange(e, setFieldValue)}
                                        onBlur={handleBlur}
                                    />
                                    {errors.newPassword && touched.newPassword && (
                                        <div className="error">
                                            {errors.newPassword}
                                        </div>
                                    )}
                                </div>
                                <div className="col-md-6 offset-md-6">
                                    <Input type="password"
                                        placeholder="Confirm New Password"
                                        name="confirmNewPassword"
                                        label="Confirm New Password"
                                        isMandatory
                                        autoComplete="off"
                                        value={values.confirmNewPassword}
                                        onChange={e => customHandleChange(e, setFieldValue)}
                                        onBlur={handleBlur}
                                    />
                                    {errors.confirmNewPassword && touched.confirmNewPassword && (
                                        <div className="error">
                                            {errors.confirmNewPassword}
                                        </div>
                                    )}
                                </div>
                                <div className="col-12 d-flex justify-content-end w-100">
                                    {/* <Button data-toggle="collapse" variant="outline">Cancel</Button> */}
                                    <Button type="submit" style={{ marginRight: '0' }}>OK</Button>



                                </div>
                            </div>
                        </Form>
                    )}
                </Formik>
            </Accrodion>
            {showAlert ? (
                <Alert
                    alertType={toast.alertType}
                    show={showAlert}
                    dismiss={true}
                    onClose={() => setShowAlert(!showAlert)}
                >
                    {toast.message}
                </Alert>
            ) : null}
        </div>
    )
}

export default ChangePassword
