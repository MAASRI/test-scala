import React from 'react';
import {
  BiHomeAlt,
  BiFolderPlus,
  BiAddToQueue,
  BiSearchAlt,
  BiCollection,
} from 'react-icons/bi';
import { RiRemoteControl2Line } from 'react-icons/ri';
import { BsCardChecklist, BsBriefcase, BsListUl } from 'react-icons/bs';
import {
  AiOutlineBulb,
  AiOutlineMail,
  AiOutlineDatabase,
} from 'react-icons/ai';
import { MdViewModule, MdAssignmentTurnedIn, MdSecurity } from 'react-icons/md';
import { FiUser, FiUsers, FiServer, FiGrid } from 'react-icons/fi';
import { SiMinutemailer } from 'react-icons/si';
import { GrDocumentConfig } from 'react-icons/gr';
import { ImStack, ImFileText2 } from 'react-icons/im';
import { FaSearchengin } from 'react-icons/fa';
import { RiLogoutBoxRLine } from 'react-icons/ri';

const iconComponent = {
  FiUser: FiUser,
  RiLogoutBoxRLine: RiLogoutBoxRLine,
  BiHomeAlt: BiHomeAlt,
  BiFolderPlus: BiFolderPlus,
  ImStack: ImStack,
  FiUsers: FiUsers,
  BsListUl: BsListUl,
  FiServer: FiServer,
  FiGrid: FiGrid,
  BsCardChecklist: BsCardChecklist,
  AiOutlineBulb: AiOutlineBulb,
  MdViewModule: MdViewModule,
  ImFileText2: ImFileText2,
  SiMinutemailer: SiMinutemailer,
  BiCollection: BiCollection,
  GrDocumentConfig: GrDocumentConfig,
  BiAddToQueue: BiAddToQueue,
  BiSearchAlt: BiSearchAlt,
  RiRemoteControl2Line: RiRemoteControl2Line,
  FaSearchengin: FaSearchengin,
  BsBriefcase: BsBriefcase,
  AiOutlineMail: AiOutlineMail,
  AiOutlineDatabase: AiOutlineDatabase,
  MdAssignmentTurnedIn: MdAssignmentTurnedIn,
  MdSecurity: MdSecurity,
};

export default iconComponent;
