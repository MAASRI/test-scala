import React from 'react'
import iconComponent from './IconList'

const RenderIcon = ({ icon }) => {
  let SpecificIcon;
  const iconExist = iconComponent.hasOwnProperty(icon)
  if (iconExist === true) {
    SpecificIcon = iconComponent[icon]
  }
  else {
    SpecificIcon = iconComponent["FiUser"]
  }
  return <SpecificIcon />

}

export default RenderIcon
