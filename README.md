# Rhea Service
Entry point and container application for a micro frontends demo.

This is a React application, which renders a navigation bar, and uses React Router to select a
microfrontend to render onto the page.

# Getting started

1. Clone the repo
2. `npm install`
3. `npm start`

You can run the container on its own, but for it to actually do anything you'll
also need to be running other services.