import React, { useState, useEffect } from 'react';
import Heading from 'ui/build/components/Heading/Heading';
import Para from 'ui/build/components/Para/Para';
import Carousel from 'ui/build/components/Carousel/Carousel';
import Input from 'ui/build/components/Inputs/Input';
import Button from 'ui/build/components/Button/Button';
import PasswordIcon from '../../utils/PasswordIcon';
import ErrorMessage from '../error-page/ErrorMessage';
import { slidesData } from '../../utils/StaticData';
import Loading from '../loading/Loading';
import useFetch from '../../utils/useFetch';
import './Login.scss';

import browserBunyan from "rr-js/lib/browserBunyan.js";
import { v4 as uuid } from "uuid";
import { SERVICE_NAME } from "../../utils/StaticData";

const initialState = {
  username: '',
  email: '',
  password: '',
  termsNCondition: false,
};

function Login() {
  const [loginReqData, setLoginReqData] = useState(initialState);
  const [clientLogoURL, setLogoUrl] = useState('');
  const [subdomain, setSubdomain] = useState('');
  const [loaded, setLoaded] = useState(false);
  const [clientFound, setClientFound] = useState();
  const [flag, setFlag] = useState(false);
  const [api, setApi] = useState(null);
  const [errorMessage, setErrorMessage] = useState(null);
  const [redirectUrl, setRedirectUrl] = useState("/");
  const logger = browserBunyan({ name: SERVICE_NAME });
  const { executeFetch, data, error, isPending, success } = useFetch(
    `${process.env.REACT_APP_APIURL}`,
  );
  const onLoginReqDataChange = e => {
    setLoginReqData({
      ...loginReqData,
      [e.target.name]: e.target.value,
    });
  };

  const onTermsAndConditionChange = e => {
    setLoginReqData({
      ...loginReqData,
      [e.target.name]: e.target.checked,
    });
  };

  const handleLogin = async event => {
    event.preventDefault();
    await executeFetch({
      api: 'login',
      options: {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          emailId: loginReqData.email.toLowerCase(),
          password: loginReqData.password,
          subdomain,
        }),
      },
      onSuccess: response => {
        const rrid = uuid();
        if (response.statusCode === 200) {
          logger.info(rrid, SERVICE_NAME, "Login API success", { emailId: loginReqData.email.toLowerCase(), subdomain });
          localStorage.setItem('isLoggedIn', true);
          localStorage.setItem('loginResponse', JSON.stringify(response));
          setRedirectUrl(JSON.parse(JSON.stringify(response.responseData.data.roleData.permissions[0].redirectUrl)));
          setFlag(true);
        }
        if (response.statusCode >= 400) {
          logger.warn(rrid, SERVICE_NAME, "Login API warning", { emailId: loginReqData.email.toLowerCase(), subdomain, response });
          setErrorMessage(response.message);
        }
      },
      onError: error => {
        const rrid = uuid();
        logger.error(rrid, SERVICE_NAME, "Login API error", { emailId: loginReqData.email.toLowerCase(), subdomain, error });
      },
    });
  };

  useEffect(() => {
    let hostname = window.location.origin;
    let domain = hostname.split(`${window.location.protocol}//`)[1];
    let subdomain = domain.split('.')[0];
    const callApi = async () => {
      await executeFetch({
        api: 'verify-subdomain',
        options: {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({ subdomain }),
        },
        onSuccess: response => {
          const rrid = uuid();
          if (response.statusCode === 200) {
            logger.info(rrid, SERVICE_NAME, "Subdomain API success", { subdomain });
            localStorage.setItem('logoUrl', response.data[0].clientLogoURL);
            localStorage.setItem('clientName', response.data[0].clientName);
            setLogoUrl(response.data[0].clientLogoURL);
            setSubdomain(subdomain);
            setClientFound(true);
            setLoaded(true);
          }
          if (response.statusCode === 404) {
            logger.warn(rrid, SERVICE_NAME, "Subdomain API warning", { subdomain, response });
            setClientFound(false);
            setLoaded(true);
          }
        },
        onError: error => {
          const rrid = uuid();
          logger.error(rrid, SERVICE_NAME, "Subdomain API error", { subdomain, error });
          console.log('Error Occured:', error)
        },
      });
    };
    callApi();
  }, []);

  const [textConfig, setTextConfig] = useState({
    variant: 'outlined',
    name: 'password',
    type: 'password',
    placeholder: 'Enter Password',
    label: 'Password',
  });

  const handleClick = config => {
    if (config.type === 'password') {
      setTextConfig({
        ...textConfig,
        type: 'text',
      });
    } else {
      setTextConfig({
        ...textConfig,
        type: 'password',
      });
    }
  };

  return (
    <>
      {!loaded ? (
        <Loading />
      ) : flag ? (
        window.location.replace(redirectUrl)
      ) : clientFound === true ? (
        <div className="df-login-container container-fluid">
          <div className="row">
            <div className="col-12 d-flex justify-content-start p-2">
              {clientLogoURL ? (
                <img
                  src={clientLogoURL}
                  alt={'Client logo'}
                  style={{ height: '84px' }}
                ></img>
              ) : (
                <img
                  src="https://azure-production.s3.amazonaws.com/DF_Logo.png"
                  alt={'Client logo'}
                  style={{ height: '84px' }}
                ></img>
              )}
            </div>
          </div>

          <div className="row">
            <div className="col-lg-5 col-md-12 col-sm-12 col-xs-12">
              <div className="row mt-4 df-login-left-top">
                <div className="col-lg-10 col-md-7 col-sm-7 col-xs-12 mt-4">
                  <Heading variant="h2" className="df-welcome-heading">
                    Welcome to {subdomain}
                  </Heading>
                  {/* <Para className="d-flex justify-content-center" style={{ marginBottom: '0' }}>
                    To keep connected with us, please login with your
                  </Para>
                  <Para className="d-flex justify-content-center" style={{ marginBottom: '0' }}>
                    personal information with email address and password
                  </Para> */}
                </div>
              </div>
              <form onSubmit={handleLogin}>
                <div className="row df-login-left-bottom">
                  <div className="col-lg-10 col-md-7 col-sm-7 col-xs-12">
                    <div className="mt-3 d-flex justify-content-center">
                      <div className="df-error-section">
                        {errorMessage && (
                          <div className="df-error-message">{errorMessage}</div>
                        )}
                      </div>
                    </div>
                    <div className="mt-3 d-flex justify-content-center">
                      <Input
                        variant="outlined"
                        type="email"
                        name="email"
                        label="Email"
                        placeholder="Enter Email"
                        autoFocus
                        value={loginReqData.email}
                        onChange={e => onLoginReqDataChange(e)}
                        autoComplete="off"
                      />
                    </div>
                    <div className="mt-3 d-flex justify-content-center">
                      <Input
                        value={loginReqData.password}
                        {...textConfig}
                        onChange={e => onLoginReqDataChange(e)}
                      >
                        <PasswordIcon
                          textConfig={textConfig}
                          onClick={handleClick}
                        />
                      </Input>
                    </div>
                    {/* <div className="mt-4 d-flex justify-content-start">
                      <div>
                        <Checkbox
                          size="md"
                          name="termsNCondition"
                          checked={loginReqData.termsNCondition}
                          onChange={e => onTermsAndConditionChange(e)}
                        />
                      </div>
                      <div className="ml-1">
                        <Para>I agree to the </Para>
                      </div>
                      <div className="ml-1">
                        <Anchor>terms & conditions</Anchor>
                      </div>
                    </div> */}
                    <div className="mt-4 d-flex justify-content-center">
                      <Button type="submit">Sign In</Button>
                    </div>
                  </div>
                </div>
              </form>
            </div>

            <div className="col-lg-7 col-md-12 col-sm-12 col-xs-12 df-login-right">
              <Carousel
                data={slidesData}
                style={{ width: '100% !important', right: '0 !important' }}
              />
            </div>
          </div>
        </div>
      ) : (
        <ErrorMessage
          errorCode="404"
          errorName="Page Not Found"
          details="Subdomain doesn't exist."
        />
      )}{' '}
    </>
  );
}

export default Login;