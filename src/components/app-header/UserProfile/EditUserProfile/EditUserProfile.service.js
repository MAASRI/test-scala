import axios from 'axios';

const umApiUrl = process.env.REACT_APP_APIURL;

let loginData, sessionToken;
if (localStorage.getItem('loginResponse')) {
  loginData = JSON.parse(localStorage.getItem('loginResponse'));
  sessionToken = loginData.responseData.sessionToken;
}

let headers = {
  'Authorization': sessionToken,
  'Content-Type': 'application/json'
}

export const getUserList = (data) => {
  return axios.post(`${umApiUrl}/user/list`, data, { headers: headers })
}

export const updateUserDataByID = (data) => {
  return axios.put(`${umApiUrl}/user/update`, data, { headers: headers })
}

export const changePasswordMethod = (data) => {
  return axios.put(`${umApiUrl}/change-password`, data, { headers: headers })
}